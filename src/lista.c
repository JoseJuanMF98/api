#include "lista.h"
#include <stdio.h>
#include <stdlib.h>

Lista *lista_nueva(void){
    Lista *L;
    L = (Lista *) malloc(sizeof(Lista));
    L->inicio = NULL;
    return L;

}


void lista_muestra(const Lista *L){
    Nodo *i;
    for (i = L->inicio; i != NULL; i = i->siguiente)
        printf("%f ", i->val);
    printf("\n");

}
void lista_libera(Lista *L){
    Nodo *n, *m, *p;
    if(L->inicio == NULL)
        return;
    if(L->inicio->siguiente == NULL)
        return;
        
    n = L->inicio;
    while (n != NULL){
        p = m;
        m = n;
        n = n->siguiente;
    }

    free(m);
    p->siguiente = NULL;
    lista_libera(L);
    
}

void lista_agrega(Lista *L, float val){
    Nodo *n, *m;
    if (L->inicio == NULL){
        L->inicio = (Nodo *) malloc(sizeof(Nodo));
        L->inicio->val = val;
        L->inicio->siguiente = NULL;
        return;
    }

    n = L->inicio;

    while (n != NULL){
        m = n;
        n = n->siguiente;
    }

    m->siguiente = (Nodo *) malloc(sizeof(Nodo));
    m->siguiente->siguiente = NULL;
    m->siguiente->val = val;
  

}

///////////////LISTA INT

Lista1 *lista_nueva1(void){
    Lista1 *A;
    A = (Lista1 *) malloc(sizeof(Lista1));
    A->inicio1 = NULL;
    return A;

}
void lista_libera1(Lista1 *A){
    Nodo1 *n, *m, *p;
    if(A->inicio1 == NULL)
        return;
    if(A->inicio1->siguiente1 == NULL)
        return;
        
    n = A->inicio1;
    while (n != NULL){
        p = m;
        m = n;
        n = n->siguiente1;
    }

    free(m);
    p->siguiente1 = NULL;
    lista_libera1(A);
    
}
void lista_agrega1(Lista1 *A, int va){
    Nodo1 *n, *m;
    if (A->inicio1 == NULL){
        A->inicio1 = (Nodo1 *) malloc(sizeof(Nodo1));
        A->inicio1->va = va;
        A->inicio1->siguiente1 = NULL;
        return;
    }

    n = A->inicio1;

    while (n != NULL){
        m = n;
        n = n->siguiente1;
    }

    m->siguiente1 = (Nodo1 *) malloc(sizeof(Nodo1));
    m->siguiente1->siguiente1 = NULL;
    m->siguiente1->va = va;
  

}


void lista_muestra1(const Lista1 *A){
    Nodo1 *i;
    for (i = A->inicio1; i != NULL; i = i->siguiente1)
        printf("%d ", i->va);
    printf("\n");

}
